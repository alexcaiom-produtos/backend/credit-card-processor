package br.com.massuda.alexander.biometria.facial.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Import;

import br.com.massuda.alexander.spring.framework.infra.web.config.Configuracao;

@SpringBootApplication
@Import({Configuracao.class, br.com.massuda.alexander.spring.framework.infra.config.Configuracao.class})
public class BiometriaFacialJpaApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(BiometriaFacialJpaApplication.class, args);
	}
    
	@Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(BiometriaFacialJpaApplication.class);
    }

}
