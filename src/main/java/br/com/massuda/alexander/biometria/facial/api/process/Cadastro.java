/**
 * 
 */
package br.com.massuda.alexander.biometria.facial.api.process;

import br.com.massuda.alexander.biometria.facial.api.model.Biometria;

/**
 * @author Alex
 *
 */
public interface Cadastro {

	void setProximo(Cadastro processo);
	Biometria cadastrar(Biometria o);

}
