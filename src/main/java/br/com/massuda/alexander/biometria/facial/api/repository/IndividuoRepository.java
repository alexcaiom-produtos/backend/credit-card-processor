/**
 * 
 */
package br.com.massuda.alexander.biometria.facial.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import br.com.massuda.alexander.biometria.facial.api.model.Individuo;

/**
 * @author Alex
 *
 */
@Repository
public interface IndividuoRepository extends JpaRepository<Individuo, Long>, JpaSpecificationExecutor<Individuo> {

}
