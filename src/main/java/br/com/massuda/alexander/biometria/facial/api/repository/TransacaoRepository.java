/**
 * 
 */
package br.com.massuda.alexander.biometria.facial.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import br.com.massuda.alexander.biometria.facial.api.model.Transacao;

/**
 * @author Alex
 *
 */
@Repository
public interface TransacaoRepository extends JpaRepository<Transacao, Long>, JpaSpecificationExecutor<Transacao> {

}
