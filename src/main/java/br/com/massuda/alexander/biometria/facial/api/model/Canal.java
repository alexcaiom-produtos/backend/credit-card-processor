/**
 * 
 */
package br.com.massuda.alexander.biometria.facial.api.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * @author Alex
 *
 */
@Entity
@Table
public class Canal {

	@Id
	private Long id;
	private String codigo;
	private String descricao;
	private String produto;
	@OneToMany(fetch = FetchType.LAZY)
	private List<Transacao> transacoes;
	@OneToMany(fetch = FetchType.LAZY)
	private List<ConfiguracaoPorCanal> configuracoes;

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getProduto() {
		return produto;
	}
	public void setProduto(String produto) {
		this.produto = produto;
	}
}
