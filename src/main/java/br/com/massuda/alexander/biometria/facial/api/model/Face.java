/**
 * 
 */
package br.com.massuda.alexander.biometria.facial.api.model;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * @author Alex
 *
 */
@Entity
@Table
public class Face {

	@Id
	private Long id;
	private String referencia;
	private LocalDateTime data;
	@Transient
	String face;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getReferencia() {
		return referencia;
	}
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}
	public LocalDateTime getData() {
		return data;
	}
	public void setData(LocalDateTime data) {
		this.data = data;
	}
	public String getFace() {
		return face;
	}
	public void setFace(String face) {
		this.face = face;
	}

}
