/**
 * 
 */
package br.com.massuda.alexander.biometria.facial.api.process;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.massuda.alexander.biometria.facial.api.model.Biometria;
import br.com.massuda.alexander.biometria.facial.api.process.handler.CadastroIndividuo;

/**
 * @author Alex
 *
 */
@Service
public class ProcessoCadastro {

	@Autowired
	CadastroIndividuo cadastroIndividuo;
	
	public void cadastrar(Biometria o) {
		definirCadeiaResponsabilidades();
		cadastroIndividuo.cadastrar(o);
	}

	private void definirCadeiaResponsabilidades() {
		
	}

}
