/**
 * 
 */
package br.com.massuda.alexander.biometria.facial.api.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import br.com.massuda.alexander.biometria.facial.api.enums.IndividuoStatus;

/**
 * @author Alex
 *
 */
@Entity
@Table
public class Individuo  {
	
	@Id
	private Long id;
	private IndividuoStatus status;
	private String cpf;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public IndividuoStatus getStatus() {
		return status;
	}
	public void setStatus(IndividuoStatus status) {
		this.status = status;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

}
