/**
 * 
 */
package br.com.massuda.alexander.biometria.facial.api.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.massuda.alexander.biometria.facial.api.model.Biometria;

/**
 * @author Alex
 *
 */
@Service
public class BiometriaFacialService {
	
	@Autowired
	private CadastroService cadastro;
	@Autowired
	private AutenticacaoService autenticacao;

	public void cadastrar(Biometria o) {
		cadastro.cadastrar(o);
	}

	public void autenticar(Biometria o) {
		autenticacao.autenticar(o);
	}

	
	
}
