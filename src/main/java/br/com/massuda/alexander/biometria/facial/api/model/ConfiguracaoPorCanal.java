/**
 * 
 */
package br.com.massuda.alexander.biometria.facial.api.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author Alex
 *
 */
@Entity
@Table
public class ConfiguracaoPorCanal {

	@Id
	private Long id;
	@ManyToOne(fetch = FetchType.EAGER)
	private Configuracao config;
	@ManyToOne(fetch = FetchType.EAGER)
	private Canal canal;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Configuracao getConfig() {
		return config;
	}
	public void setConfig(Configuracao config) {
		this.config = config;
	}
	public Canal getCanal() {
		return canal;
	}
	public void setCanal(Canal canal) {
		this.canal = canal;
	}
}
