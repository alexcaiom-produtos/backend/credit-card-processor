/**
 * 
 */
package br.com.massuda.alexander.biometria.facial.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import br.com.massuda.alexander.biometria.facial.api.model.Face;

/**
 * @author Alex
 *
 */
@Repository
public interface FaceRepository extends JpaRepository<Face, Long>, JpaSpecificationExecutor<Face> {

}
