/**
 * 
 */
package br.com.massuda.alexander.biometria.facial.api.process;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.massuda.alexander.biometria.facial.api.model.Biometria;
import br.com.massuda.alexander.biometria.facial.api.process.handler.ValidacaoBureau;
import br.com.massuda.alexander.biometria.facial.api.process.handler.ValidacaoClienteJaCadastrado;
import br.com.massuda.alexander.biometria.facial.api.process.handler.ValidacaoEsteganografia;
import br.com.massuda.alexander.biometria.facial.api.process.handler.ValidacaoLiveness;

/**
 * @author Alex
 *
 */
@Service
public class ProcessoValidacaoCadastro  {
	
	@Autowired
	private ValidacaoEsteganografia esteganografia;
	@Autowired
	private ValidacaoBureau bureau;
	@Autowired
	private ValidacaoLiveness liveness;
	@Autowired
	private ValidacaoClienteJaCadastrado clienteCadastrado;

	public void validar(Biometria o) {
		definirCadeiaResponsabilidade();
		esteganografia.validar(o);
	}

	private void definirCadeiaResponsabilidade() {
		esteganografia.setProximo(bureau);
		bureau.setProximo(liveness);
		liveness.setProximo(clienteCadastrado);
	}
	
}
