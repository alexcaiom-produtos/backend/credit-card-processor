package br.com.massuda.alexander.biometria.facial.api.enums;

public enum IndividuoStatus {

	AUTENTICAVEL,
	CONFLITO,
	ERRO,
	INATIVO;
	
}
