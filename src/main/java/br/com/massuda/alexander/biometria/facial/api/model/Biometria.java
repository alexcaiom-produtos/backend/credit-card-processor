/**
 * 
 */
package br.com.massuda.alexander.biometria.facial.api.model;

import java.util.List;

/**
 * @author Alex
 *
 */
public class Biometria {
	
	List<Face> fotos;
	Transacao transacao;
	Individuo individuo;
	
	public List<Face> getFotos() {
		return fotos;
	}
	public void setFotos(List<Face> fotos) {
		this.fotos = fotos;
	}
	public Transacao getTransacao() {
		return transacao;
	}
	public void setTransacao(Transacao transacao) {
		this.transacao = transacao;
	}
	public Individuo getIndividuo() {
		return individuo;
	}
	public void setIndividuo(Individuo individuo) {
		this.individuo = individuo;
	}
	@Override
	public String toString() {
		return "Biometria [fotos=" + fotos + ", transacao=" + transacao + ", individuo=" + individuo + "]";
	}

}
