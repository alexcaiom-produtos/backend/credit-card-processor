package br.com.massuda.alexander.biometria.facial.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.massuda.alexander.biometria.facial.api.model.Biometria;
import br.com.massuda.alexander.biometria.facial.api.service.BiometriaFacialService;

@CrossOrigin
@RestController
@RequestMapping("/face")
public class BiometriaFacialController {
	
	@Autowired
	BiometriaFacialService service;
	
	@PostMapping
	public void incluir(@RequestBody Biometria o) {
		service.cadastrar(o);
	}
	
	
	@PutMapping
	public void alterar(@RequestBody Biometria o) {
		service.autenticar(o);
	}
	
//	@GetMapping
//	public void getLogs() {
//		service.autenticar(o);
//	}
	
}
