package br.com.massuda.alexander.biometria.facial.api.enums;

/**
 * @author Alex
 *
 */
public enum StatusLogin {

	NORMAL, BLOQUEADO;
	
}
