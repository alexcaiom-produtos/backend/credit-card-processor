package br.com.massuda.alexander.biometria.facial.api.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.massuda.alexander.biometria.facial.api.model.Biometria;
import br.com.massuda.alexander.biometria.facial.api.process.ProcessoCadastro;
import br.com.massuda.alexander.biometria.facial.api.process.ProcessoValidacaoCadastro;

@Service
public class CadastroService {

	@Autowired
	private ProcessoValidacaoCadastro validacoes;
	@Autowired
	private ProcessoCadastro processo;
	
	public void cadastrar(Biometria o) {
		validacoes.validar(o);
		processo.cadastrar(o);
	}
	
}