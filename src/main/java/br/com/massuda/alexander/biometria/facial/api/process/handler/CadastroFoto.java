/**
 * 
 */
package br.com.massuda.alexander.biometria.facial.api.process.handler;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import br.com.massuda.alexander.biometria.facial.api.model.Biometria;
import br.com.massuda.alexander.biometria.facial.api.model.Transacao;
import br.com.massuda.alexander.biometria.facial.api.process.Cadastro;
import br.com.massuda.alexander.biometria.facial.api.repository.TransacaoRepository;
import br.com.massuda.alexander.spring.framework.infra.utils.Imagem;

/**
 * @author Alex
 *
 */
@Service
public class CadastroFoto implements Cadastro {
	
	Cadastro proximo;
	@Autowired
	TransacaoRepository repository;
	@Autowired
	private Imagem imagem;
	@Value("${server.servlet.context-path}")
	private String api;
	
	@Override
	public Biometria cadastrar(Biometria o) {
		Transacao transacaoBD = repository.save(o.getTransacao());
		o.setTransacao(transacaoBD);
		proximo.cadastrar(o);
		return o;
	}

	@Override
	public void setProximo(Cadastro processo) {
		this.proximo = processo;
	}
	

	
	private void tratarFotosInformadas(Biometria b) {
		if (Objects.nonNull(b.getFotos()) && !b.getFotos().isEmpty()) {
			api = api.replace("/", "");
			for (int indiceFoto = 0; indiceFoto < b.getFotos().size(); indiceFoto++) {
				LocalDateTime data = LocalDateTime.now();
//				String timestamp = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss").format(data.getTime());
				String timestamp = DateTimeFormatter.ofPattern(DateTimeFormatter.ISO_DATE_TIME.toString()).format(data);
				String local = "c:/base64/{api}/{cpf}/{transacao}/{timestamp}.jpg"
						.replace("{api}", api)
						.replace("{cpf}", b.getIndividuo().getCpf())
						.replace("{transacao}", b.getTransacao().getTransacao())
						.replace("{timestamp}", timestamp);
				String nomeFTP = "/public_ftp/{api}/images/{login}/{timestamp}.jpg"
						.replace("{api}", api)
						.replace("{cpf}", b.getIndividuo().getCpf())
						.replace("{transacao}", b.getTransacao().getTransacao())
						.replace("{timestamp}", timestamp);
				imagem.converterParaArquivo(b.getFotos().get(indiceFoto).getFace(), local, nomeFTP);
				b.getFotos().get(indiceFoto).setReferencia(nomeFTP);
				b.getFotos().get(indiceFoto).setData(data);
			}
		}
	}
}
