package br.com.massuda.alexander.biometria.facial.api.process.handler;

import org.springframework.stereotype.Component;

import br.com.massuda.alexander.biometria.facial.api.model.Biometria;
import br.com.massuda.alexander.biometria.facial.api.process.ProcessoValidacao;

@Component
public class ValidacaoBureau implements ProcessoValidacao {

	private ProcessoValidacao proximo;

	@Override
	public void setProximo(ProcessoValidacao processo) {
		this.proximo = processo;

	}

	@Override
	public void validar(Biometria o) {
		// TODO Auto-generated method stub
		proximo.validar(o);
	}

}
