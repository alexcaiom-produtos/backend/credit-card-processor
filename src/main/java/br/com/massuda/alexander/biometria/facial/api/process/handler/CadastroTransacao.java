/**
 * 
 */
package br.com.massuda.alexander.biometria.facial.api.process.handler;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.massuda.alexander.biometria.facial.api.model.Biometria;
import br.com.massuda.alexander.biometria.facial.api.model.Face;
import br.com.massuda.alexander.biometria.facial.api.process.Cadastro;
import br.com.massuda.alexander.biometria.facial.api.repository.FaceRepository;

/**
 * @author Alex
 *
 */
@Service
public class CadastroTransacao implements Cadastro {
	
	Cadastro proximo;
	@Autowired
	FaceRepository repository;
	
	@Override
	public Biometria cadastrar(Biometria o) {
		List<Face> facesBD = new ArrayList<>();
		o.getFotos().forEach(f -> {
			Face faceBD = repository.save(f);
			facesBD.add(faceBD);
		});
		o.setFotos(facesBD);
		proximo.cadastrar(o);
		return o;
	}

	@Override
	public void setProximo(Cadastro processo) {
		this.proximo = processo;
	}
}
