package br.com.massuda.alexander.biometria.facial.api.process.handler;

import org.springframework.stereotype.Service;

import br.com.massuda.alexander.biometria.facial.api.model.Biometria;
import br.com.massuda.alexander.biometria.facial.api.process.ProcessoValidacao;

@Service
public class ValidacaoClienteJaCadastrado implements ProcessoValidacao {

	private ProcessoValidacao proximo;

	@Override
	public void setProximo(ProcessoValidacao processo) {
		this.proximo = processo;
	}

	@Override
	public void validar(Biometria o) {
		executar(o);
		this.proximo.validar(o);
	}

	private void executar(Biometria o) {
		// TODO Auto-generated method stub
		
	}
	
}
