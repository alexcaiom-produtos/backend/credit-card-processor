/**
 * 
 */
package br.com.massuda.alexander.biometria.facial.api.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * @author Alex
 *
 */
@Entity
@Table
public class Configuracao {

	@Id
	private Long id;
	private String nome;
	private Boolean ativa;
	private String valor;
	@OneToMany(fetch = FetchType.LAZY)
	private List<ConfiguracaoPorCanal> configs;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Boolean getAtiva() {
		return ativa;
	}
	public void setAtiva(Boolean ativa) {
		this.ativa = ativa;
	}
	public String getValor() {
		return valor;
	}
	public void setValor(String valor) {
		this.valor = valor;
	}
	public List<ConfiguracaoPorCanal> getConfigs() {
		return configs;
	}
	public void setConfigs(List<ConfiguracaoPorCanal> configs) {
		this.configs = configs;
	}
	
}
