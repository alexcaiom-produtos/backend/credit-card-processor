/**
 * 
 */
package br.com.massuda.alexander.biometria.facial.api.process;

import br.com.massuda.alexander.biometria.facial.api.model.Biometria;

/**
 * @author Alex
 *
 */
public interface ProcessoValidacao {
	void setProximo(ProcessoValidacao processo);
	void validar(Biometria o);
}
